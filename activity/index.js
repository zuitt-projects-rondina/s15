
function oddEvenChecker(number){

		if(typeof number === "number") {

			if (number % 2 == 0){

				return "The number is even"

			}	else if(number % 2 ==1) {

				return "The number is odd"
			}

		} else {

			return "Invalid input"
		}

}

let checkNum = oddEvenChecker (3);
console.log(checkNum);


function budgetChecker(budget){

	if(typeof budget ==="number") {
		if(budget > 40000) {
			return "You are over the budget"

		} else if(budget <= 40000) {

			return "You have resources left"
		}


	} else {
			return "Invalid input"
	}
}

let checkBudget = budgetChecker(500);
console.log(checkBudget);

