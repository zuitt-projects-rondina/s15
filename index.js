//ASSIGNMENT OPERATORS

//Basic assignment operator (=)
	//It allow us to assign a value to a variable
	//example

	let variable = "initial value";

// Mathematical Operators (addition(+), subtraction(-), multiplication(*), divisiom(/), modulo(%))

let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

	//Addition Assignment Operator(+=)
	//left operand is the variable of the left side of the operator
	//right operand is the variable of the right side of the operator
	// num1 = num1 + num4 (re-assigned the value of num1 with the result of num1+ num4)

	num1 += num4 ;
	console.log(num1);	
		// result : 45

	num1 +=55;
	console.log(num1);
		// result : 100
	console.log(num4) //to check if the right operand changed its value
		//result : 40


	let string1 = "Boston";
	let string2 = "Celtics";

	string1 += string2;
	console.log(string1);
		//result : BostonCeltics

   //15 += num1;
   //console.log(num1);
    	//result : error
    	//note : do not use assignment operator when the left operand is just data


   //Subtraction Assignment Operator

   num1 -= num2;
   console.log(num1);
   		//result : 90
   num1 -= num4;
   console.log(num1);
   		//result : 50
   num1 -= 10;
   console.log(num1);
   		//result : 40
   num1 -= string1;
   console.log(num1);
   		//result : NaN (Not a number)- because string1 is an alphanumeric string
   string1 -= string2;
   console.log(string1);
   		//result : NaN


   	//Multiplication Assignment Operator

   	num2 *= num3 ;
   	console.log(num2);
   		//result : 40
   	num2 *= 5;
   	console.log(num2);
   		//result : 200


   	//DIVISION ASSIGNMENT OPERATOR

   	num4 /= num3;
   	console.log(num4);
   		//resuk : 10
   	num4 /= 2;
   	console.log(num4);
   		//resukt : 5


   	// MODULO OPERATOR (%)

/*   	console.log(num2);
   			//result : 200
   	console.log(num4);
   			//result : 5
   	console.log(num4 % num2);*/

   	let y = 15;
   	let x = 2;
   	console.log(y % x);


 //MATHEMATICAL OPERATINS - we follow MDAS

 let mdasResult = 1 + 2 - 3 * 4 / 5;
 console.log(mdasResult);
 		//result = 0.6


 //PEMDAS
  let pemdasResult = 1 + (2 - 3) * (4 / 5);
  console.log(pemdasResult);
  		//result : 0.2


  // INCREMENT AND DECREMENT 
 	//2 kinds of Incrementation, pre-fix and post-fix

  //pre-fix	
  let z = 1;
  ++z;
  console.log(z);
  		// result : 2

 //post-fix
 z++;
 console.log(z);
 		// result is 3 = the value of z is added with 1 immediately returned
 console.log(z++);
 		// result is 3 = with post-fix incremantation the previous value of the variable is returned first before incrementing
 console.log(z);
  		//result is 4	= new value is returned

  //PRE-FIX AND POST-FIX DECREMENTATION

  	console.log(--z);
  			// result is 3 = the result is subtracted by 1 and returned immediately
  	console.log(z--);
  			// result is 3 = return the previous value
  	console.log(z);
  			// result is 2 = return the new value

  // Comparison Operator
  	// this is used to compare the value of the left and right operand
  	// note that comaprison operatorrs returns a value

  	// EQUALIY
  	console.log(1==1);
  		//result : true

  	//we can save the rsult of a comparison in a variable

  	let isSame = 55==55;
  	console.log(isSame);

   console.log(1 == "1");
   		//result : true
   		// note  loose equality 

   	console.log(0 == false);
   		// result : true
   		// note = false is converted into a number and the equivalent of false is 0

   	console.log(1 == true);
   		// result is true
   		//note = true is equal to 1
   	console.log('1' == 1);
   		//result is true
   	console.log(true =='true');
   		//result is false
   		// note = true =1 and 'true' is a string

   		/*
			With loose comparison operator (==) values are compared , if the operands do not have the same type, 
			it will be force to coerced before comparing values

			If either operand is a number or boolean . the operands are converted into numbers
		
   		*/

   	// STRICT EQUALITY OPERATOR (===)
   		console.log(true === '1');
   			//result : false
   			// note = strictly check both the value and the data type

   		console.log("Johnny" === "Johnny");
   			//result : true
   			// note : same data type and same value
   		console.log("lisa" === "Lisa")
   			//result : false
   			// note: differences with letters casing can affect the value



// INEQUALITY OPERATORS
	//LOOSE INEQUALITY OPERATORS (!=)
			//Checks if the operands are not equal and or have different values

	console.log("1" != 1);
			//result : false
			// "1" is converted to number 1
			// 1 equals to 1

	console.log("Rose" != "Jennie");
			// result : true
	console.log(false != 0);
			//result is false
	console.log(true != "true")
			// result is true		


// STRICT INEQUALITY OPERATOR (!==)
			//Checks if the operands are not equal and or have different values STRICTLY


console.log("5" !== 5);
		//result is true
		//note : they have a different data types
console.log(5 !== 5);
		//result is false
		// note : they have the same value and data type
console.log("true" !== true);
		//results is true
		//note : different data types


/*MINI ACTIVITY*/

let name1 = "Juan";
let name2 = "Shane";
let name3 = "Peter";
let name4 = "Jack";

let number = 50;
let number2 = 60;
let numString1 = "50";
let numString2 = "60";

console.log(numString1 == number);
			//result : true
console.log(numString1 === number);
			//result : false
console.log(numString1 != number);
			//result : false
console.log(name4 !== name3);
			//result : true
console.log(name1 == "juan");
			//result : false
console.log(name1 === "Juan");
			//result : true



// RELAIONAL COMPARISON OPERATORS
	// A COMPARISON OPERATORS WLL CHECK THE RELATIONSHIP BETWEEN OPERANDS

	let q = 500;
	let r = 700;
	let w = 8000;
	let numString3 = "5500";

	//Greater than (>)

	console.log(q > r);
		// result is false
	console.log(w > r);
		// result is true

	// Less Than (<)
	console.log(w < q);
		//result : false
	console.log(q < 1000);
		//result is true
	console.log(numString3 < 6000);
		// result is true;
	console.log(numString3 < "Jose");
		// result : true - this is erratic

	// Greater than or equal to (>=)

	console.log(w >= 8000);
		//result true
	console.log(r >= q);
		//result true

	//Less than or equal to (<=)

	console.log(q <= r);
		//result true
	console.log(w <= q);
		//result is false


	// LOGICAL OPERATORS
		//AND OPERATOR (&&)
			// ALL OPERANDS MUST BE TRUE OR WILL RESULT TO TRUE

		let isAdmin = false;
		let isRegistered = true;
		let isLegalAge = true;

		let authorization1 = isAdmin && isRegistered;
		console.log(authorization1);
					// result : false
		let authorization2 = isLegalAge && isRegistered;
		console.log(authorization2);
					//result : true

		let requiredLevel = 95;
		let requiredAge = 18;

		let authorization3 = isRegistered && requiredLevel === 25;
		console.log(authorization3);
				// result : false
				// note : true && 95 === 25 which will result to false
		let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
		console.log(authorization4);
				// result : true;

		let userName = "gamer2001";
		let userName2 = "shadow1991";
		let userAge = 15;
		let userAge2 = 30;

		let registration1 = userName.length > 8 && userAge >= requiredAge
		console.log(registration1);
				//return : false

		let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
		console.log(registration2);

		// OR OPERATOR (|| - DOUBLE PIPE)
			// OR OPERATOR RETURNS TRUE IF ONE OF THE OPERAND IS TRUE


		let userLevel = 100;
		let userLevel2 = 65;

		let guildRequirement1 = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
		console.log(guildRequirement1);
				// result : false

		let guildRequirement2 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
		console.log(guildRequirement2)
				// result : true

		let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
		console.log(guildAdmin);
				// return : false

		// NOT OPERATOR
			// TURN A BOOLEAN VALUE INTO THE OPPOSITE VALUE

		console.log(!isRegistered);
					// result:false

		console.log(!guildAdmin);
					// result: true


		// IF-ELSE STATEMENTS 
				// IF STATEMENT WILL RUN A BLOCK OF CODE IF THE CONDITION SPECIFIED IS TRUE OR RESULT TO TRUE

		/*if(true){

			alert("We run an if condition!")

		};*/

		let userName3 = "crusader_1993";
		let userLevel3 = 25;
		let userAge3 = 20;

		if(userName3.length > 10){

			console.log("Welcome to Game Online")
		};

		if(userLevel3 >= requiredLevel) {

			console.log("You are qualified to join the guild")
		}; 

		if(userName3.length >= 10 && isRegistered && isAdmin) {

			console.log("THANK YOU FOR JOINING THE ADMIN")
		};

		// ELSE STATEMENT 
			// else statement  will run if condition is false or will return to false


		if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){

				console.log("Thank you for joining the noob guild")

		}	else	{

				console.log("You are to strong to be a noob")
		};

		// ELSE-IF 
			//executes a statement if the previous or the original condition is false or resulted to false

		if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {

				console.log("Thank you for joining the noob guild")

		} else if(userLevel3 > 3) {

				console.log("You are too strong to be a noob")
		} else if (userAge3 < requiredAge){
				console.log("You are too  young  to join the guild")

		} else if (userName3.length < 10) {
			console.log("Username too short ")
		} else {

			console.log("Unable to join")
		};


		// if-else inside the function

		function addNum(num1,num2) {

			if(typeof num1 === "number" && typeof num2 === "number")  {

				console.log("Run only if both arguments passed are number types");
				console.log(num1 + num2);	

			} else {
				console.log( "One or both of the arguments are not a number")
			};
		};

		addNum(5, 2);

			function login(username, password) {
				if(typeof username=== "string" && typeof password === "string")
					console.log("Both arguments are string");

				//neseted if
					if(username.length >= 8 && password.length >= 8){
						alert("Thank you for logging in")

					}else if (username.length < 8) {
						alert("Username too short")

					} else if (password.length < 8 ){
						alert ("password is too short")

					}else {
				console.log("One of the argumenst is not string")
			}

		} 


		login("theTinker","tinkerbell");


// SWITCH STATEMENT
	// IS AN ALTERNATIVE TO AN IF , ELSE IF 

		//SYNTAX 

		/*
			switch(expression/condition) {
					case value:
						statement;
						break;
					default:
						statement;
						break;

			}
			
		*/

		let hero = "Hercules";

		switch(hero) {

			case "Jose Rizal":
			console.log("National Hero of the PH");
			break;

			case "George Washington":
			console.log("Hero of the Americn Revolution")
			break;

			case "Hercules":
			console.log("Legendary hero of the greeks")
			break;

		};


	function roleChecker(role) {

		switch(role) {
			case "Admin":
				console.log("Welcome Admin");
				break;

			case "User":
				console.log("Welcome User");
				break;

			case "Guest":
				console.log("Welcome Guest");
				break;	

			default :
				console.log("Invalid role");			

		}
	}

	roleChecker("Admin");


	// function with if-else and return keyword


function gradeEvaluator(grade) {

	if( grade >= 90){

		return "A"

	} else if(grade >= 80) {
		return "B"

	} else if (grade >= 71) {
		return "C"

	} else if (grade <= 70) {
		return "F"

	} else {
		return "invalid grade" 
	}

};

let letterDistinction = gradeEvaluator (85);
console.log(letterDistinction);
	// result : B

// TERNARY OPERATOR 
		// A shorthand of writing if else statement

		/*
			syntax

			condition ? if-statement : else-statement
		*/

		let price = 5000;

		price > 1000 ? console.log("Price is over 1000") :
				console.log("Price is less than 1000");

		let villain = "Harvey Dent";

		/*villain === "Harvey Dent" ? console.log("You were supposed to be the chosen one")
*/			//NOTE - ELSE STATEMENT IN TERNARY IS REQUIRED
		
		villain === "Two Face" ? console.log("You lived enough to be villain") :
			console.log("Not quite villain yet");

		// NOTE THAT TERNARY OPERATORS ARE NOT MEANT FOR COMPLEX STATEMENT TREES, THE MAIN ADVANTAGE OF TERNARY 
		// 	IS NOT BECAUSE ITS SHORT,  RATHER TERNARY  IMPLICITLY RETURN OR IT IS BECAUSE IT CAN RETURN WITHOUT RETURN KEYWORD 


		let robin1 = " Dick Grayson";
		let currentRobin = "Tim Drake";

		let isFirstRobin = currentRobin === robin1 ? true : false;

		console.log(isFirstRobin);
				//result : false

		// ELSE-IF WITH TERNARY

		let a = 7;
		a === 5 ? console.log("A") : console.log(a === 10 ? console.log("A is 10") :
					console.log("A is not 5 or 10"));